package ru.kuzin.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.kuzin.tm.model.Session;

@Repository
public interface ISessionRepository extends IUserOwnedRepository<Session> {
}