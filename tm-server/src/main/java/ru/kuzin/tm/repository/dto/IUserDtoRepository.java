package ru.kuzin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.kuzin.tm.dto.model.UserDTO;

@Repository
public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    boolean existsByLogin(@NotNull String login);

    boolean existsByEmail(@NotNull String email);

}