package ru.kuzin.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.exception.AbstractException;

public final class EndpointException extends AbstractException {

    public EndpointException(@NotNull String message) {
        super(message);
    }

}