package ru.kuzin.tm.repository;

import ru.kuzin.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Task> tasks = new LinkedHashMap<>();

    private TaskRepository() {
    }

    {
        add(new Task("Task 1"));
        add(new Task("Task 2"));
        add(new Task("Task 3"));
    }

    public void create() {
        add(new Task("New Task " + System.currentTimeMillis()));
    }

    public void add(Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(Task task) {
        tasks.put(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(String id) {
        return tasks.get(id);
    }

    public void removeById(String id) {
        tasks.remove(id);
    }

}